const express = require("express");
const cors = require("cors");

const app = express();
const ReportGenerator = require('./app/helpers/ReportGenerator')

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Covid-19 Test application." });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/test.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
