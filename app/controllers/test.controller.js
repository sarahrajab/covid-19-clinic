const db = require("../models");
const helper = require("../helpers/test.helper");
const Test = db.test;
const ReportGenerator = require('../helpers/reportGenerator');
const nodemailer = require('nodemailer');
const emailConfig = require("../config/email.config");
const sequelize = require("sequelize");

/**
 * Save the JotForms form in the database
 */
jotFormWebhook = (req, res) => {
    // Validate request
    if (!req.body.q293_confirmationCode) {
        res.status(400).send({
            message: "Submission ID can not be empty!"
        });
        return;
    }
    //Get the detailes object address
    let address = helper.addressParser(req.body.q366_address);

    // Save Test to Database
    Test.create({
        address_street: address.streetName,
        address_city: address.city,
        address_state: address.state,
        address_zip: address.postalCode,
        address_full: req.body.q366_address,
        jotform_submission_id: req.body.q293_confirmationCode,
        patient_email: req.body.q225_email,
        patient_name: req.body.q3_name.first + " " + req.body.q3_name.last,
        patient_phone: req.body.q5_phoneNumber.full,
        patient_sex: req.body.q315_sex,
        patient_dob: req.body.q338_dob.year + "-" + req.body.q338_dob.month + "-" + req.body.q338_dob.day,
        site_location: req.body.q341_site_location,
        submission_date: req.body.q261_date.year + "-" + req.body.q261_date.month + "-" + req.body.q261_date.day,
        test_dcl_code: req.body.event_id,
        result: "pending",
        report_status: "pending"
    })
    .then(test => {
        res.send(test);
    })
    .catch(err => {
        res.status(500).send({ message: err.message || "Some error occurred while saving the Test."});
    });
};

/**
 * Get a list of the tests according to the saved clinic
 */
findAll = (req, res) => {
    Test.findAll({
        where: {
            site_location: req.user.location
        },
        raw: true
      })
    .then(tests => {
        res.send(tests);
    })
    .catch(err => {
        res.status(500).send({ message: err.message || "Some error occurred while retrieving tests."});
    })
}

/**
 * Update the result of the test to: [Pending, Positive, Negative]
 * @param {integer} id The test id
 */
updateResult = (req, res) => {
    const id = req.params.id;

    //Validate result status
    if (!req.body.result) {
        res.status(400).send({
            message: "Result can not be empty!"
        });
        return;
    }
    if(!['pending', 'positive', 'negative'].includes(req.body.result.trim().toLowerCase())) {
        res.status(400).send({
            message: "Result status should only have one of the statuses: Pending, Positive or Negative."
        });
        return;
    }

    Test.update({result: req.body.result}, {
        where: { id: id }
    }).then(num => {
        if (num == 1) {
            res.send({
                message: "Test status was updated successfully."
            });
            } else {
            res.send({
                message: `Cannot update Test with id=${id}. Maybe Test was not found or req.body is empty!`
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Error updating Test with id=" + id
        });
    });
};

/**
 * Generates the Report PDF and send it to the patient email
 * @param {integer} id The test id
 */
sendReport = (req, res) => {
    const id = req.params.id;

    Test.findByPk(id, {raw: true})
    .then(test => {
        if(test){
            const reportGenerator = new ReportGenerator(test)
            let pdfPath = reportGenerator.generate();

            let mail = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: emailConfig.email,
                    pass: emailConfig.password
                }
            });

            let mailOptions = {
                from: emailConfig.email,
                to: test.patient_email,
                // to: "sarah.rajab94@gmail.com",
                subject: "Covid-19 Test Result",
                text: 'Covid-19 Test Result',
                attachments: [{   
                    filename: 'covid_19_test_report.pdf',
                    path: pdfPath
                }]
            };
            
            mail.sendMail(mailOptions, (error, info) => {
                if (error) {
                    res.status(500).send({
                        message: "Unable to send the email" + error
                    });
                } else {
                    console.log("SENT")
                    //Update the report_date of the test
                    Test.update({
                        report_status: "sent",
                        report_date: new Date()
                    }, {
                    where: {
                        id: id
                    }
                    }).then(num => {
                        res.send({
                            message: 'Email sent: ' + info.response
                        });
                    }).catch(err => {
                        console.log(err)
                        res.status(500).send({
                            message: "Error updating report_status of Test with id=" + id
                        });
                    })
                }
            });
        } else {
            return res.status(400).send({
                message: `Cannot send the report of the test id=${id}. The test were not found!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: "Unable to find the test " + err
        });
    })
}

module.exports = {jotFormWebhook, findAll, updateResult, sendReport};