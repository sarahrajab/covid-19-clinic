const e = require("express");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    
    //Get the user again and add it to the request
    User.findByPk(decoded.id, {raw: true}).then(user => {
        if(user){
            delete user.password;
            req.user = user;
        } else { //The user were deleted
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        next();
    });
  });
};

const authJwt = {
  verifyToken: verifyToken
};

module.exports = authJwt;