// Convert string full address to an detailed object address
addressParser = (stringAddress) => {
    let address = stringAddress.split(/\r\n/);
    let result = {
        city: "",
        state: "",
        postalCode: "",
        streetName: ""
    };
    address.forEach(part => {
        parts = part.split(":");
        result[camelize(parts[0])] = parts[1].trim();
    })
    return result;
}


camelize = (str) => {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
}
  
module.exports = {addressParser};