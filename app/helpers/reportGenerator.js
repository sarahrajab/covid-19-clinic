const PDFGenerator = require('pdfkit')
const fs = require('fs')
const qr = require('qr-image');

class ReportGenerator {
    constructor(test) {
        this.test = test
        this.beginningOfPage = 50
        this.endOfPage = 550
    }

    generateHeaders(doc) {
        doc.image('./app/data/templates/logo.png', 50, 20, { width: 150})
            .fillColor('#000')
            .fontSize(10)
            .text(`1824 W Wilson Ave, Chicago`, 380, 40)
            .text(`IL 60640`, 380, 55)
            .text(`PHONE: 844-777-1237`, 380, 70)
            .font('Helvetica-Bold').text(`CLIA ID NUMBER ${this.test.jotform_submission_id}`, 380, 85)
            .moveDown()

        doc.moveTo(this.beginningOfPage,110)
            .lineTo(this.endOfPage,110)
            .stroke()
                
        doc.fontSize(8).text(`ReqId: ${this.test.test_dcl_code} `, 60, 120)
        .text(`DOB: ${this.test.patient_dob}`, 270, 120)
        .text(`COLLECTION DATE: ${new Date(this.test.submission_date).toLocaleString('en-US', {year: 'numeric', month: 'numeric', day: 'numeric'})}`, 380, 120)
        .text(`PATENT NAME: ${this.test.patient_name}`, 60, 135)
        .text(`SEX: ${this.test.patient_sex}`, 270, 135)
        .text(`REPORTED DATE: ${new Date().toLocaleString('en-US', {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })}`, 380, 135)


        doc.moveTo(this.beginningOfPage,150)
            .lineTo(this.endOfPage,150)
            .stroke()

    }

    generateContent(doc){
        doc.rect(50, 160, 500, 25).fillAndStroke('#ddd', '#000')
        .fill('#000').stroke()
        .fontSize(8)
        .font('Helvetica-Bold').text("TEST", 60, 170, {lineBreak: false})
        .font('Helvetica-Bold').text("RESUTL", 300, 170, {lineBreak: false})
        .font('Helvetica-Bold').text("FLAG", 430, 170, {lineBreak: false});

        doc.fontSize(10).font('Helvetica-Bold').text("ANTIGEN COVID-19 SARS-COV19 - NP", 60, 200, {lineBreak: false})
        .font('Helvetica-Bold').text(`${this.test.result}`, 300, 200, {lineBreak: false})
        .font('Helvetica-Bold').text(`NORMAL`, 430, 200, {lineBreak: false})

        doc.moveTo(this.beginningOfPage,230)
            .lineTo(this.endOfPage,230)
            .stroke()

        doc.fontSize(14).font('Helvetica-Bold').text("INTERPRETATION OF RESULT: ",this.beginningOfPage, 270);
        doc.fontSize(12).font('Helvetica-Bold').text("What does a negative test result mean?",this.beginningOfPage, 300)
        .fontSize(10).font('Helvetica').text("A negative antigen test means that SARS-CoV-2 viral proteins were not detected. However, a negative test does not rule out COVID-19. If there is still concern that a person has COVID-19 after a negative antigen test, then that person should be tested again with a molecular test.",this.beginningOfPage, 320);
        doc.fontSize(12).font('Helvetica-Bold').text("What does a positive test result mean?",this.beginningOfPage, 370)
        .fontSize(10).font('Helvetica').text("A positive antigen test means that the person being tested has an active COVID-19 infection.",this.beginningOfPage, 390);
        doc.fontSize(12).font('Helvetica-Bold').text("Antigen Test?",this.beginningOfPage, 420)
        .fontSize(10).font('Helvetica').text("Antigen tests look for pieces of proteins that make up the SARSCoV-2 virus to determine if the person has an active infection.",this.beginningOfPage, 440);
        
        var qrString = qr.imageSync('https://shyp.studio', { type: 'png' });
        doc
            .image(qrString, 200, 460, { width: 200})
            .text("REPORT COMPLETE", 250, 660)
            .text(`Report Date: ${new Date().toLocaleString('en-US', {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })}`, 225, 690)
    }

    generate() {
        let theOutput = new PDFGenerator 

        const fileName = `./app/data/reports_pdf/Report_ ${this.test.jotform_submission_id}.pdf`

        // pipe to a writable stream which would save the result into the same directory
        theOutput.pipe(fs.createWriteStream(fileName))

        this.generateHeaders(theOutput)

        theOutput.moveDown()

        this.generateContent(theOutput)

        // write out file
        theOutput.end()

        return fileName;
    }
}

module.exports = ReportGenerator