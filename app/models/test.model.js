module.exports = (sequelize, Sequelize) => {
    const Test = sequelize.define("tests", {
        address_street: {
            type: Sequelize.STRING
        },
        address_city: {
            type: Sequelize.STRING
        },
        address_zip: {
            type: Sequelize.STRING
        },
        address_full: {
            type: Sequelize.STRING
        },
        jotform_submission_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        patient_email: {
            type: Sequelize.STRING
        },
        patient_name: {
            type: Sequelize.STRING
        },
        patient_phone: {
            type: Sequelize.STRING
        },
        patient_sex: {
            type: Sequelize.STRING
        },
        patient_dob: {
            type: Sequelize.DATEONLY
        },
        site_location: {
            type: Sequelize.STRING,
            allowNull: false
        },
        submission_date: {
            type: Sequelize.DATEONLY
        },
        test_dcl_code: {
            type: Sequelize.STRING
        },
        result: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: "pending"
        },
        report_status: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: "pending"
        },
        report_date: {
            type: Sequelize.DATE
        }
    });
    return Test;
};