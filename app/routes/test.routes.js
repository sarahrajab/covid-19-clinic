const { authJwt } = require("../middleware");
const controller = require("../controllers/test.controller");

module.exports = app => {
    var router = require("express").Router();

    //Jotform webhook endpoint to save tests
    router.post(
        "/jotFormWebhook",
        controller.jotFormWebhook
    );

    //Get all tests of the auth clinic
    router.get(
        "/",
        [authJwt.verifyToken],
        controller.findAll
    );

    // Update Test Status with id
    router.put(
        "/updateResult/:id",
        [authJwt.verifyToken],
        controller.updateResult
    );

    //Send result report to the patient
    router.post(
        "/sendReport/:id",
        [authJwt.verifyToken],
        controller.sendReport
    );

    app.use('/api/tests', router);
};